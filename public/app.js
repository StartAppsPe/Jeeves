var app = angular.module('Jeeves', ['ngRoute', 'ngAnimate']);

// ROUTING ===============================================
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'page-login.html',
            controller: 'loginController'
        })
        .when('/config', {
            templateUrl: 'page-config.html',
            controller: 'configController'
        })
        .when('/servers', {
            templateUrl: 'page-servers.html',
            controller: 'serversController'
        })
        .when('/servers/:name', {
            templateUrl: 'page-server.html',
            controller: 'serverController'
        });
});

app.controller('mainController', function($rootScope, $scope, $http, $log, $location, Session, AuthService) {
    Session.load();
    $rootScope.serverName = "ITLAB";

    var goBack = function() {
        $location.path("/servers");
    };
    $scope.goBack = goBack;

    var openConfig = function() {
        $location.path("/config");
    };
    $scope.openConfig = openConfig;

    var doLogout = function() {
        AuthService.logout().then(function(user) {
            $scope.$apply(function() {
                $location.path("/");
            });
        }).catch(function(err) {
            $scope.error = "Error! Wrong user";
        });
    };
    $scope.doLogout = doLogout;
});

app.controller('loginController', function($rootScope, $scope, $http, $log, $location, AuthService) {
    $rootScope.pageClass = 'page-login';

    var doLogin = function(credentials) {
        AuthService.login(credentials).then(function(user) {
            $scope.$apply(function() {
                $location.path("/servers");
            });
        }).catch(function(err) {
            $scope.error = "Error! Wrong user";
        });
    };
    $scope.doLogin = doLogin;
});

app.controller('configController', function($rootScope, $scope, $http, $log, $location) {
    $scope.pageClass = 'page-config';

    var loadData = function() {
        $http.get("/jeevesapi/config").then(
            function success(response) {
                $scope.config = response.data;
                $log.log("data:", response);
            },
            function error(response) {
                $scope.error = response.data;
                $log.error("error:", response);
            }
        );
    };
    $scope.loadData = loadData;

    var addUser = function() {
        var config = $scope.config;
        config.users.push({});
        $scope.config = config;
    };
    $scope.addUser = addUser;

    var deleteUser = function(username) {
        var config = $scope.config;
        for (var i in config.users) {
            if (config.users[i].username == username) {
                config.users.splice(i, 1);
            }
        }
        $scope.config = config;
    };
    $scope.deleteUser = deleteUser;

    var saveConfig = function(newConfig) {
        swal({
            title: "Confirm action",
            text: "Save configuration and users",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function() {
            $http.post("/jeevesapi/config", {
                "config": newConfig
            }).then(
                function success(response) {
                    $scope.result = response.data;
                    $log.log("data:", response);
                    loadData();
                    swal("Success!", JSON.stringify(response.data), "success");
                },
                function error(response) {
                    $scope.error = response.data;
                    $log.error("error:", response);
                    loadData();
                    swal("Error!", JSON.stringify(response.data), "error");
                }
            );
        });
    };
    $scope.saveConfig = saveConfig;

    loadData();
});

app.controller('serversController', function($rootScope, $scope, $http, $log, $location) {
    $rootScope.pageClass = 'page-servers';

    var loadData = function() {
        $http.get("/jeevesapi/servers/").then(
            function success(response) {
                $scope.servers = response.data;
                $log.log("data:", response);
            },
            function error(response) {
                $scope.error = response.data;
                $log.error("error:", response);
            }
        );
    };
    $scope.loadData = loadData;

    var openServer = function(name) {
        $location.path("/servers/" + name);
    };
    $scope.openServer = openServer;

    var newServer = function() {
        swal({
            title: "Confirm action",
            text: "New server",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Server name"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need input a name!");
                return false;
            }
            $scope.$apply(function() {
                $location.path("/servers/" + inputValue);
            });
            swal("Created!", "Server " + inputValue + " was created", "success");
        });
    };
    $scope.newServer = newServer;

    var autoReload = setInterval(function() {
        loadData();
    }, 1000 * 30);
    $scope.$on("$destroy", function() {
        clearInterval(autoReload);
    });

    loadData();
});

app.controller('serverController', function($rootScope, $scope, $http, $log, $location, $routeParams) {
    $rootScope.pageClass = 'page-server';

    var loadData = function() {
        $http.get("/jeevesapi/servers/" + $routeParams.name).then(
            function success(response) {
                $scope.server = response.data;
                $log.log("data:", response);
            },
            function error(response) {
                if (response.status == 401) {
                    $scope.error = "unauthorized";
                    $log.error("error:", "unauthorized");
                    $location.path("/");
                    return;
                }
                $scope.error = response.data;
                $log.error("error:", response);
            }
        );
    };
    $scope.loadData = loadData;

    var saveServer = function(newServer) {
        swal({
            title: "Confirm action",
            text: "Save server",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function() {
            $http.post("/jeevesapi/servers/" + $routeParams.name, {
                "server": newServer
            }).then(
                function success(response) {
                    $scope.result = response.data;
                    $log.log("data:", response);
                    loadData();
                    swal("Success!", JSON.stringify(response.data), "success");
                },
                function error(response) {
                    $scope.error = response.data;
                    $log.error("error:", response);
                    loadData();
                    swal("Error!", JSON.stringify(response.data), "error");
                }
            );
        });
    };
    $scope.saveServer = saveServer;

    var deleteServer = function(newServer) {
        swal({
            title: "Confirm action",
            text: "Delete server (permanent)",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function() {
            $http.delete("/jeevesapi/servers/" + $routeParams.name).then(
                function success(response) {
                    $scope.result = response.data;
                    $log.log("data:", response);
                    loadData();
                    swal("Success!", JSON.stringify(response.data), "success");
                },
                function error(response) {
                    $scope.error = response.data;
                    $log.error("error:", response);
                    loadData();
                    swal("Error!", JSON.stringify(response.data), "error");
                }
            );
        });
    };
    $scope.saveServer = saveServer;

    var performAction = function(action) {
        swal({
            title: "Confirm action",
            text: "Perform \"" + action + "\" on server \"" + $routeParams.name + "\"",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function() {
            $http.post("/jeevesapi/servers/" + $routeParams.name + "/perform?action=" + action).then(
                function success(response) {
                    $scope.result = response.data;
                    $log.log("data:", response);
                    loadData();
                    swal("Success!", JSON.stringify(response.data), "success");
                },
                function error(response) {
                    if (response.status == 401) {
                        $log.error("error:", "unauthorized");
                        $location.path("/");
                        return;
                    }
                    $scope.error = response.data;
                    $log.error("error:", response);
                    loadData();
                    swal("Error!", JSON.stringify(response.data), "error");
                }
            );
        });
    };
    $scope.performAction = performAction;

    loadData();
});

app.factory('AuthService', function($http, Session) {
    var authService = {};
    authService.login = function(credentials) {
        return new Promise(function(resolve, reject) {
            $http.post('/jeevesapi/login', credentials).then(function(response) {
                if (!response.data.user || !response.data.token) return reject("Bad response");
                Session.create(response.data.user, response.data.token);
                return resolve(response.data.user);
            }).catch(function(error) {
                return reject(error);
            });
        });
    };
    authService.logout = function() {
        return new Promise(function(resolve, reject) {
            $http.post('/jeevesapi/logout').then(function(response) {
                if (!response.data.result) return reject("Bad response");
                if (response.data.result == "success") {
                    Session.destroy();
                    return resolve(response.data.result);
                } else {
                    return reject(response.data.result);
                }
            }).catch(function(error) {
                return reject(error);
            });
        });
    };
    authService.isAuthenticated = function() {
        return !!Session.user;
    };
    return authService;
});

app.service('Session', function($http, $rootScope) {
    this.load = function() {
        if (!sessionStorage || !sessionStorage.session) return;
        var obj = JSON.parse(sessionStorage.session);
        if (!obj || !obj.user || !obj.token) return;
        for (var prop in obj) this[prop] = obj[prop];
        $http.defaults.headers.common.Authorization = 'Bearer ' + this.token.value;
        $http.defaults.headers.common.token = this.token.value;
        $rootScope.currentUser = this.user;
    };
    this.create = function(user, token) {
        this.user = user;
        this.token = token;
        sessionStorage.session = JSON.stringify(this);
        $http.defaults.headers.common.Authorization = 'Bearer ' + this.token.value;
        $http.defaults.headers.common.token = this.token.value;
        $rootScope.currentUser = this.user;
    };
    this.destroy = function() {
        this.user = null;
        this.token = null;
        sessionStorage.session = null;
        $http.defaults.headers.common.Authorization = null;
        $http.defaults.headers.common.token = null;
        $rootScope.currentUser = null;
    };
});
