1. Update Instance
```
sudo yum update
```

2. Create SSH key and register on git (optional)
```
https://gitlab.com/help/ssh/README
```

3. Install git
```
sudo yum install git-all
```

4. Install node
```
curl --silent --location https://rpm.nodesource.com/setup_5.x | bash -
```

5. Install PM2
```
npm install pm2 -g
```

6. Change permission of /srv
```
sudo chmod -R ugo+rw
```

7. Clone repository to desired location
```
cd /srv
[sudo] git clone https://gitlab.com/StartAppsPe/Jeeves.git
```

8. Install Jeeves dependencies
```
cd /srv/Jeeves
[sudo] npm install
```

9. Start Jeeves using PM2
```
[sudo] pm2 start /srv/Jeeves/server.js -i 0 -n "Jeeves"
```

10. Forward port 80 to 8000 in iptables
```
sudo iptables -I INPUT -p tcp --dport 8000 --syn -j ACCEPT
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to 8000
sudo /etc/init.d/iptables save
```
Or
```
sudo firewall-cmd --zone=public --add-port=8000/tcp --permanent
sudo firewall-cmd --zone=external --add-forward-port=port=80:proto=tcp:toport=8000 --permanent
```

11. Copy and rename default config file
```
cd /srv/Jeeves
[sudo] cp config-example.json config.json
```
