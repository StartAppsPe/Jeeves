/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var authAdapter = require('./authAdapter.js');
var userLogs = require('./storageModels/userLogsModel.js');
var configModel = require('./storageModels/configModel.js');
var gitAdapter = require('./serviceAdapters/gitAdapter.js');
var pm2Adapter = require('./serviceAdapters/pm2Adapter.js');
var tomcatAdapter = require('./serviceAdapters/tomcatAdapter.js');
var phpAdapter = require('./serviceAdapters/phpAdapter.js');
var staticAdapter = require('./serviceAdapters/staticAdapter.js');

var servers = function(req) {
    return new Promise(function(resolve, reject) {
        configModel.config().then(function(config) {
            return config.servers;
        }).then(function(servers) {
            for (var i in servers) {
                var cleanName = servers[i].git.replace('http://', '').replace('https://', '');
                cleanName = cleanName.replace('www.', '').replace('.git', '');
                var cleanNameSplit = cleanName.split('@');
                cleanName = (cleanNameSplit.length > 1 ? cleanNameSplit[1] : cleanName);
                servers[i].repo = cleanName;
                servers[i].status = {};
                servers[i].status.jeeves = (servers[i].enabled ? 'green' : 'red');
            }
            return servers;
        }).then(function(servers) {
            for (var i in servers) {
                if (!req || !servers[i].token) continue;
                var baseUrl = req.protocol + '://' + req.get('host') + "/jeevesapi/hook/" + servers[i].name;
                servers[i].hooks = {};
                servers[i].hooks.start = baseUrl + "?action=start&access_token=" + servers[i].token;
                servers[i].hooks.stop = baseUrl + "?action=stop&access_token=" + servers[i].token;
                servers[i].hooks.restart = baseUrl + "?action=restart&access_token=" + servers[i].token;
                servers[i].hooks.pull = baseUrl + "?action=pull&access_token=" + servers[i].token;
                servers[i].hooks.update = baseUrl + "?action=update&access_token=" + servers[i].token;
            }
            return servers;
        }).then(function(servers) {
            var gitPromises = [];
            var servicePromises = [];
            for (var i in servers) {
                gitPromises.push(gitAdapter.status(req, servers[i]));
                var serviceAdapter = serviceAdapterForServer(servers[i]);
                servicePromises.push(serviceAdapter.status(req, servers[i]));
            }
            userLogs.logConsole(req, "All", "Git", "Status", "Run");
            userLogs.logConsole(req, "All", "Service", "Status", "Run");
            return [servers, Promise.all(gitPromises), Promise.all(servicePromises)];
        }).spread(function(servers, gitStatus, serviceStatus) {
            for (var i in servers) {
                servers[i].status.git = gitStatus[i];
                servers[i].status.service = serviceStatus[i];
            }
            resolve(servers);
        }).catch(function(err) {
            reject(err);
        });
    });
};

var serviceAdapterForServer = function(server) {
    if (server.service.type) {
        switch (server.service.type) {
            case "node":
                return pm2Adapter;
            case "java":
                return tomcatAdapter;
            case "php":
                return phpAdapter;
            case "static":
                return staticAdapter;
        }
    }
    return staticAdapter;
};

var performAction = function(req, serverName, action) {
    return new Promise(function(resolve, reject) {
        servers().then(function(servers) {
            if (action == "restartRoutes") {
                action = "restart";
                serverName = "Jeeves";
            }
            var server = null;
            for (var i in servers) {
                if (servers[i].name == serverName) {
                    server = servers[i];
                    break;
                }
            }
            if (!server) throw Error("No server found");
            var serviceAdapter = serviceAdapterForServer(server);
            switch (action) {
                case "pull":
                    return gitAdapter.pull(req, server);
                case "clone":
                    return gitAdapter.clone(req, server);
                case "push":
                    return gitAdapter.push(req, server);
                case "replace":
                    return gitAdapter.replace(req, server);
                case "start":
                    console.log("Stopping","QWERTYUIOPA1");
                    return serviceAdapter.start(req, server);
                case "stop":
                    console.log("Stopping","QWERTYUIOPB1");
                    return serviceAdapter.stop(req, server);
                case "restart":
                    return serviceAdapter.restart(req, server);
                case "update":
                    var promises = [];
                    promises.push(gitAdapter.pull(req, server));
                    promises.push(serviceAdapter.restart(req, server));
                    return Promise.all(promises);
                case "deploy":
                    return serviceAdapter.deploy(req, server);
                default:
                    throw Error("No action found");
            }
        }).then(function(result) {
            return resolve(result);
        }).catch(function(err) {
            console.log("Stopping","QWERTYUIOPZZZZZZZZZZZZZZZ1",err);
            return reject(err);
        });
    });
};

var express = require('express');
var router = express.Router();

var passport = require('passport');
router.use(passport.initialize());

router.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    next();
});

router.get('/servers', authAdapter.isAuthenticatedAny, function(req, res) {
    servers(req).then(function(servers) {
        return res.status(200).json(servers);
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.get('/servers/:name', authAdapter.isAuthenticatedAny, function(req, res) {
    servers(req).then(function(servers) {
        for (var i in servers) {
            if (servers[i].name == req.params.name) {
                return servers[i];
            }
        }
        throw Error("No server found");
    }).then(function(result) {
        return res.status(200).json(result);
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.post('/servers/:name/perform', authAdapter.isAuthenticatedUser, function(req, res) {
    if (!req.query.action) {
        return res.status(500).json({
            "error": "No action recieved"
        });
    }
    performAction(req, req.params.name, req.query.action).then(function(result) {
        return res.status(200).json(result);
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.post('/servers/:name', authAdapter.isAuthenticatedAdmin, function(req, res) {
    var newServer = req.body.server;
    if (!newServer) {
        return res.status(500).json({
            "error": "No values sent"
        });
    }
    configModel.config().then(function(config) {
        var server = null;
        for (var i in config.servers) {
            if (newServer.name == config.servers[i].name) {
                server = config.servers[i];
                break;
            }
        }
        if (!server) {
            server = new configModel.Server();
            server.name = newServer.name;
            server.git = newServer.git;
            config.addServer(server);
        }
        return [config, server];
    }).spread(function(config, server) {
        server.owner = newServer.owner;
        for (var prop1 in newServer.service) {
            server.service[prop1] = newServer.service[prop1];
        }
        for (var prop2 in newServer.route) {
            server.route[prop2] = newServer.route[prop2];
        }
        return config.save();
    }).then(function(config) {
        return res.status(200).json("Configuration Saved!");
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.delete('/servers/:name', authAdapter.isAuthenticatedMaster, function(req, res) {
    configModel.config().then(function(config) {
        var server = null;
        for (var i in config.servers) {
            if (req.params.name == config.servers[i].name) {
                server = config.servers[i];
                config.servers.splice(i, 1);
                break;
            }
        }
        if (!server) throw "No server found";
        return config.save();
    }).then(function(config) {
        return res.status(200).json("Configuration Saved!");
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.get('/config', authAdapter.isAuthenticatedMaster, function(req, res) {
    configModel.config().then(function(config) {
        return res.status(200).json(config);
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

router.post('/config', authAdapter.isAuthenticatedMaster, function(req, res) {
    var newConfig = req.body.config;
    if (!newConfig) {
        return res.status(500).json({
            "error": "No values sent"
        });
    }
    configModel.config().then(function(config) {
        var oldConfig = JSON.parse(JSON.stringify(config));
        for (var prop in newConfig) {
            config[prop] = newConfig[prop];
        }
        for (var i in config.users) {
            var user1 = config.users[i];
            var oldUser1 = null;
            for (var j in oldConfig.users) {
                if (user1.username == oldConfig.users[j].username) {
                    oldUser1 = oldConfig.users[j];
                    break;
                }
            }
            if (!oldUser1 || user1.password != oldUser1.password) {
                user1.password = authAdapter.hashWithSalt(user1.username, user1.password);
            }
        }
        return config.save();
    }).then(function(config) {
        return res.status(200).json("Configuration Saved!");
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

// Authorization
router.post('/login', function(req, res) {
    authAdapter.login(req, req.body.username, req.body.password).then(function(result) {
        return res.status(200).json(result);
    }).catch(function(err) {
        return res.status(401).json({
            "error": err
        });
    });
});

// Authorization
router.post('/logout', function(req, res) {
    authAdapter.logout(req).then(function(result) {
        return res.status(200).json(result);
    }).catch(function(err) {
        return res.status(401).json({
            "error": err
        });
    });
});

// Authorization
router.all('/hook/:name', authAdapter.isAuthenticatedDirect, function(req, res) {
    performAction(req, req.params.name, req.query.action).then(function(result) {
        return res.status(200).json(result);
    }).catch(function(err) {
        return res.status(500).json({
            "error": err
        });
    });
});

exports.router = router;
