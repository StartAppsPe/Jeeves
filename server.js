/*jshint node: true */
"use strict";

var version = '0.2.0';

var express = require('express');
var app = express();

// use body parser so we can get info from POST and/or URL parameters
var bodyParser = require('body-parser');

// Admin code
var adminAdapter = require('./adminAdapter.js');
var userLogs = require('./storageModels/userLogsModel.js');
var configModel = require('./storageModels/configModel.js');

var httpProxy = require('http-proxy');
var proxyServer = function(server, serverRoot) {
    if (!server.enabled) return;
    if (!server.route || !server.route.type) return;
    if (!server.route.path) return;
    var serverPath;
    switch (server.route.type) {
        case "web":
            serverPath = server.route.destination;
            userLogs.logConsole("router", server.name, "Route", "Web", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.web(req, res);
            });
            break;
        case "port":
            serverPath = "http://localhost:" + server.route.port;
            userLogs.logConsole("router", server.name, "Route", "Port", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.web(req, res);
            });
            break;
        case "socket":
            serverPath = server.route.destination;
            userLogs.logConsole("router", server.name, "Route", "Socket", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.ws(req, res);
            });
            break;
        case "php":
            serverPath = "http://localhost:8008" + server.service.phpPath;
            userLogs.logConsole("router", server.name, "Route", "PHP", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.web(req, res);
            });
            break;
        case "java":
            serverPath = "http://localhost:8080/" + server.name;
            userLogs.logConsole("router", server.name, "Route", "Java", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.web(req, res);
            });
            break;
        case "static":
            serverPath = serverRoot + server.name + server.route.destination;
            userLogs.logConsole("router", server.name, "Route", "Static", server.route.path + " -> " + serverPath);
            app.use(server.route.path, express.static(serverPath));
            break;
        default:
            serverPath = server.route.destination;
            userLogs.logConsole("router", server.name, "Route", "Default", server.route.path + " -> " + serverPath);
            app.use(server.route.path, function(req, res) {
                var proxy = httpProxy.createProxyServer({
                    target: serverPath,
                    changeOrigin: true
                });
                proxy.web(req, res);
            });
    }
};


configModel.config().then(function(config) {
    for (var i in config.servers) {
        proxyServer(config.servers[i], config.serverRoot);
    }
    app.listen(process.env.PORT || config.mainPort);
    userLogs.logConsole("server", "Jeeves", "Listen", "Port " + config.mainPort);
    userLogs.logConsole("server", "Jeeves", "Init", "Success");
}).catch(function(err) {
    app.listen(process.env.PORT || 8000);
    userLogs.logConsole("server", "Jeeves", "Listen", "Port " + 8000);
    userLogs.logConsole("server", "Jeeves", "Init", "Failed");
}).finally(function() {
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use('/jeevesapi', adminAdapter.router);
    app.use('/admin', express.static(__dirname + '/public'));
});

app.all('/alive', function(req, res) {
    userLogs.logConsole(req, "Jeeves", "Alive", "Sent");
    res.json({
        'result': 'success',
        'data': 'alive',
        'version': version
    });
});
