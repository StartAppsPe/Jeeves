/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var configModel = require('../storageModels/configModel.js');
var userLogs = require('../storageModels/userLogsModel.js');
var exec = require('child_process').exec;
var request = require('request');


var status = function(req, server) {
    //userLogs.logConsole(req, server.name, "Tomcat", "Status", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return resolve('black');
        if (!server.service.type || server.service.type != "java") return resolve('blue');
        request('http://localhost:8000'+server.route.path, function(error, response, body) {
            if (error || (response.statusCode != 200 && response.statusCode != 304)) return resolve('red');
            return resolve('green');
        });
    });
};
exports.status = status;

var start = function(req, server, restarting) {
    userLogs.logConsole(req, server.name, "Tomcat", "Start", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "java") return reject("Service is not java");
        configModel.config().then(function(config) {
            exec('/usr/local/tomcat7/bin/startup.sh', function(err, stdout, stderr) {
                console.log("TC1", err);
                console.log("TC2", stdout);
                console.log("TC3", stderr);
                if (err) return reject(err);
                if (stdout != "Tomcat started.\n") return reject("Output: " + stdout);
                if (!restarting) userLogs.log(req, server.name, "Tomcat", "Start", "Success");
                return resolve("Started!");
            });
        }).catch(function(err) {
            console.log("TC4", err);
            return reject(err);
        });
    });
};
exports.start = start;

var stop = function(req, server, restarting) {
    userLogs.logConsole(req, server.name, "Tomcat", "Stop", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "java") return reject("Service is not java");
        configModel.config().then(function(config) {
            exec('/usr/local/tomcat7/bin/shutdown.sh && echo \'success\'', function(err, stdout, stderr) {
                console.log("TC1", err);
                console.log("TC2", stdout);
                console.log("TC3", stderr);
                if (err) return reject(err);
                if (stdout != "success\n") return reject("Output: " + stdout);
                if (!restarting) userLogs.log(req, server.name, "Tomcat", "Stop", "Success");
                return resolve("Stopped!");
            });
        }).catch(function(err) {
            console.log("TC4", err);
            return reject(err);
        });
    });
};
exports.stop = stop;

var restart = function(req, server) {
    userLogs.logConsole(req, server.name, "Tomcat", "Restart", "Run");
    return new Promise(function(resolve, reject) {
        start(req, server).then(function(result) {
            return stop(req, server);
        }).then(function(result) {
            userLogs.log(req, server.name, "Tomcat", "Restart", "Success");
            return resolve("Restarted!");
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.restart = restart;

var update = function(req, server) {
    userLogs.logConsole(req, server.name, "Tomcat", "Update", "Run");
    return new Promise(function(resolve, reject) {
        deploy(req, server).then(function(result) {
            return restart(req, server);
        }).then(function(result) {
            userLogs.log(req, server.name, "Tomcat", "Restart", "Success");
            return resolve("Updated!");
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.update = update;

var deploy = function(req, server) {
    userLogs.logConsole(req, server.name, "Tomcat", "Deploy", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "java") return reject("Service is not java");
        if (!server.service.javaWarLocation) return reject("No WAR location specified");
        configModel.config().then(function(config) {
            exec('rm -rf /usr/local/tomcat7/webapps/' + server.name + ' && ' +
                'rm -rf /usr/local/tomcat7/webapps/' + server.name + '.war && ' +
                'cp /srv/' + server.name + server.service.javaWarLocation + ' /usr/local/tomcat7/webapps/' + server.name + '.war && ' +
                'echo \'success\'',
                function(err, stdout, stderr) {
                    if (err) return reject(err);
                    if (stdout != "success\n") return reject("Output: " + stdout);
                    userLogs.log(req, server.name, "Tomcat", "Deploy", "Success");
                    return resolve("Deployed!");
                });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.deploy = deploy;
