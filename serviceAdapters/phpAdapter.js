/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var configModel = require('../storageModels/configModel.js');
var userLogs = require('../storageModels/userLogsModel.js');
var exec = require('child_process').exec;
var request = require('request');


var status = function(req, server) {
    //userLogs.logConsole(req, server.name, "PHP", "Status", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return resolve('black');
        if (!server.service.type || server.service.type != "php") return resolve('blue');
        request('http://localhost:8000'+server.route.path, function(error, response, body) {
            if (error || response.statusCode != 200) return resolve('red');
            return resolve('green');
        });
    });
};
exports.status = status;

var start = function(req, server, skipLog) {
    userLogs.logConsole(req, server.name, "PHP", "Start", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "php") return reject("Service is not php");
        configModel.config().then(function(config) {
            exec('httpd -k start && echo \'success\'', function(err, stdout, stderr) {
                if (err) return reject(err);
                if (stdout != "success\n") return reject("Output: " + stdout);
                if (!skipLog) userLogs.log(req, server.name, "PHP", "Start", "Success");
                return resolve("Started!");
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.start = start;

var stop = function(req, server, skipLog) {
    userLogs.logConsole(req, server.name, "PHP", "Stop", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "php") return reject("Service is not php");
        configModel.config().then(function(config) {
            exec('sudo httpd -k stop && echo \'success\'', function(err, stdout, stderr) {
                if (err) return reject(err);
                if (stdout != "success\n") return reject("Output: " + stdout);
                if (!skipLog) userLogs.log(req, server.name, "PHP", "Stop", "Success");
                return resolve("Stopped!");
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.stop = stop;

var restart = function(req, server, skipLog) {
    userLogs.logConsole(req, server.name, "PHP", "Restart", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "php") return reject("Service is not php");
        configModel.config().then(function(config) {
            exec('sudo httpd -k restart && echo \'success\'', function(err, stdout, stderr) {
                if (err) return reject(err);
                if (stdout != "success\n") return reject("Output: " + stdout);
                if (!skipLog) userLogs.log(req, server.name, "PHP", "Restart", "Success");
                return resolve("Restarted!");
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.restart = restart;

var update = function(req, server, skipLog) {
    userLogs.logConsole(req, server.name, "PHP", "Update", "Run");
    return new Promise(function(resolve, reject) {
        deploy(req, server).then(function(result) {
            return restart(req, server);
        }).then(function(result) {
            if (!skipLog) userLogs.log(req, server.name, "PHP", "Restart", "Success");
            return resolve("Updated!");
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.update = update;

var deploy = function(req, server, skipLog) {
    userLogs.logConsole(req, server.name, "PHP", "Deploy", "Run");
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "php") return reject("Service is not php");
        if (!server.service.phpPath) return reject("No PHP path specified");
        configModel.config().then(function(config) {
            exec('cp -a /srv/' + server.name + '/. /var/www/html' + server.service.phpPath + '/ && ' +
                'restorecon -r /var/www/html/ && echo \'success\'',
                function(err, stdout, stderr) {
                    if (err) return reject(err);
                    if (stdout != "success\n") return reject("Output: " + stdout);
                    if (!skipLog) userLogs.log(req, server.name, "PHP", "Deploy", "Success");
                    return resolve("Deployed!");
                });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.deploy = deploy;
