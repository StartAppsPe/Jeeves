/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var git = require('simple-git');
var fs = require("fs");

var configModel = require('../storageModels/configModel.js');
var userLogs = require('../storageModels/userLogsModel.js');

var pathForRepo = function(name) {
    return new Promise(function(resolve, reject) {
        configModel.config().then(function(config) {
            var path = config.serverRoot + name;
            return resolve(path);
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.pathForRepo = pathForRepo;

var checkPath = function(path) {
    return new Promise(function(resolve, reject) {
        try {
            fs.accessSync(path, fs.F_OK);
            return resolve(path);
        } catch (err) {
            return reject(err);
        }
    });
};
exports.checkPath = checkPath;

var updateRepoDate = function(name) {
    return new Promise(function(resolve, reject) {
        configModel.config().then(function(config) {
            var server = null;
            for (var i in config.servers) {
                if (config.servers[i].name == name) {
                    server = config.servers[i];
                    break;
                }
            }
            if (!server) throw Error("Server not found");
            server.dateUpdated = formatDate(new Date());
            return config.save();
        }).then(function(config) {
            return resolve(config);
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.updateRepoDate = updateRepoDate;

var status = function(req, server) {
    //userLogs.logConsole(req, server.name, "Git", "Status", "Run");
    return new Promise(function(resolve, reject) {
        pathForRepo(server.name).then(function(path) {
            return checkPath(path);
        }).then(function(path) {
            var repo = git(path);
            if (!repo) return resolve('black');
            repo.status(function(err, data) {
                if (err) return resolve('red');
                if (!data) return resolve('red');
                if (data.deleted && data.deleted.length > 0) return resolve('orange');
                if (data.created && data.created.length > 0) return resolve('orange');
                if (data.modified && data.modified.length > 0) return resolve('orange');
                if (data.not_added && data.not_added.length > 0) return resolve('orange');
                if (data.conflicted && data.conflicted.length > 0) return resolve('orange');
                return resolve('green');
            });
        }).catch(function(err) {
            return resolve('black');
        });
    });
};
exports.status = status;

var clone = function(req, server) {
    userLogs.logConsole(req, server.name, "Git", "Clone", "Run");
    return new Promise(function(resolve, reject) {
        pathForRepo(server.name).then(function(path) {
            var repo = git();
            repo.clone(server.git, path, function(err) {
                if (err) return reject(err);
                updateRepoDate(server.name).then(function() {
                    userLogs.log(req, server.name, "Git", "Clone", "Success");
                    return resolve("Cloned!");
                }).catch(function(err) {
                    return resolve("Cloned!!!");
                });
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.clone = clone;

var push = function(req, server) {
    userLogs.logConsole(req, server.name, "Git", "Push", "Run");
    return new Promise(function(resolve, reject) {
        pathForRepo(server.name).then(function(path) {
            return checkPath(path);
        }).then(function(path) {
            var repo = git(path);
            if (!repo) return reject("Repository not found");
            repo.add('./*').commit("Jeeves Commit")
                .push('origin', 'master', function(err) {
                    if (err) return reject(err);
                    userLogs.log(req, server.name, "Git", "Push", "Success");
                    return resolve("Pushed!");
                });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.push = push;

var pull = function(req, server) {
    userLogs.logConsole(req, server.name, "Git", "Pull", "Run");
    return new Promise(function(resolve, reject) {
        pathForRepo(server.name).then(function(path) {
            return checkPath(path);
        }).then(function(path) {
            var repo = git(path);
            if (!repo) return reject("Repository not found");
            repo.pull(function(err, data) {
                if (err) return reject(err);
                updateRepoDate(server.name).then(function() {
                    userLogs.log(req, server.name, "Git", "Pull", "Success");
                    return resolve(data);
                }).catch(function(err) {
                    return reject(err);
                });
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.pull = pull;

var replace = function(req, server) {
    userLogs.logConsole(req, server.name, "Git", "Replace", "Run");
    return new Promise(function(resolve, reject) {
        pathForRepo(server.name).then(function(path) {
            return checkPath(path);
        }).then(function(path) {
            var repo = git(path);
            if (!repo) return reject("Repository not found");
            repo.fetch().reset('hard', function(err) {
                if (err) return reject(err);
                userLogs.log(req, server.name, "Git", "Replace", "Success");
                return resolve("Replaced!");
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.replace = replace;

var formatDate = function(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return pad(day, 2) + '/' + pad(month, 2) + '/' + year;
};
var pad = function(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
};

/*app.all('/clone/', function(req, res) {
  console.log('Cloning: ', 'Starting');
  git.clone('https://gitlab.com/StartAppsPe/StartAppsKit.git', './StartAppsKit', function(err) {
      var response = {
          'result': 'success',
          'data': 'clone',
          'version': version
      };
      console.log('Master: ', response);
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', 'application/json; charset=utf-8');
      res.end(JSON.stringify(response));
    });
});
*/
