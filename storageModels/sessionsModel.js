/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var fs = require("fs");

var sessionsFilePath = __dirname + "/../sessions.json";

var sessions = function() {
    return new Promise(function(resolve, reject) {
        fs.readFile(sessionsFilePath, 'utf8', function(err, data) {
            if (err) return resolve(new SessionsFile());
            return resolve(new SessionsFile(JSON.parse(data)));
        });
    });
};
exports.sessions = sessions;

var SessionsFile = function(obj) {
    this.tokens = [];
    this.save = function() {
        var fileToSave = this;
        return new Promise(function(resolve, reject) {
            fs.writeFile(sessionsFilePath, JSON.stringify(fileToSave, null, 4), "utf8", function(err) {
                if (err) return reject(err);
                return resolve(fileToSave);
            });
        });
    };
    this.clean = function() {
        for (var i=this.tokens.length-1; i>=0; i--){
            if (new Date(this.tokens[i].expiration) < new Date()) {
                this.tokens.splice(i, 1);
            }
        }
    };
    this.findToken = function(value) {
        for (var i in this.tokens) {
            if (this.tokens[i].value == value) {
                return this.tokens[i];
            }
        }
        return null;
    };
    this.addToken = function(token) {
        this.tokens.push(token);
    };
    this.removeToken = function(value) {
        for (var i in this.tokens) {
            if (this.tokens[i].value == value) {
                var username = this.tokens[i].username;
                this.tokens.splice(i, 1);
                return username;
            }
        }
        return null;
    };
    for (var prop in obj) this[prop] = obj[prop];
};
exports.SessionsFile = SessionsFile;

var SessionToken = function(username, value, expiration, userAgent, browserSession) {
    this.username = username;
    this.value = value;
    this.expiration = expiration;
    this.userAgent = userAgent;
    this.browserSession = browserSession;
};
exports.SessionToken = SessionToken;
